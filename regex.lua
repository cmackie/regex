function match (S, patt)
    local m = coroutine.wrap(
        function() return patt(S, 1) end
    )
    for pos in m do
        if pos == string.len(S) + 1 then
            return true
        end
    end
    return false
end

function prim (str)
    return function(S, pos)
        local len = string.len(str)
        if string.sub(S, pos, pos + len - 1) == str then
            coroutine.yield(pos + len)
        end
    end
end

function alt (patt1, patt2)
    return function(S, pos)
        local m1 = coroutine.wrap(
            function() return patt1(S, pos) end
        )
        for pos in m1 do
            coroutine.yield(pos)
        end
        local m2 = coroutine.wrap(
            function() return patt2(S, pos) end
        )
        for pos in m2 do
            coroutine.yield(pos)
        end
    end
end

function seq (patt1, patt2)
    return function(S, pos)
        local btpoint = coroutine.wrap(
            function() return patt1(S, pos) end
        )
        for npos in btpoint do
            patt2(S, npos)
        end
    end
end

function zeroormore (patt)
    assert(false, "Not Implemented")
end

function optional (patt)
    assert(false, "Not Implemented")
end

function oneormore (patt)
    assert(false, "Not Implemented")
end


print(match("def", seq(alt(prim("abc"),prim("de")),prim("f"))))
print(match("cccab", seq(zeroormore(alt(prim("c"),prim("d"))),prim("ab"))))
print(match("abce", seq(seq(prim("ab"),zeroormore(alt(prim("c"),prim("d")))),prim("ce"))))
print(match("abce", seq(seq(prim("ab"),optional(prim("d"))),prim("ce"))))
print(match("abdd", seq(prim("ab"),alt(zeroormore(prim("c")),oneormore(prim("d"))))))
print(match("abdce", seq(seq(prim("ab"),oneormore(prim("d"))),prim("ce"))))
